﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace deltafile_mailkit
{
    public interface IMailInterface
    {
        void Connect(string mailServer, string mailAccount, string mailPass, int mailServerPort, bool useSsl);
        void DeleteMessege(int msgID);
        void Disconnect();
        List<EmailMessage> GetEmails();
        
    }
}
