﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit;
using MailKit.Net;
using MimeKit;


namespace deltafile_mailkit
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            try
            {
                log.Info("Started");
                string endOfLine = "";
                // read parameters from config
                var config = new Config();
                config.GetConfiguration();



                var emailClient = new MailRepository();

                emailClient.Connect(config.MailHost, config.MailUser, config.MailPwd, config.MailPort, true);
                List<EmailMessage> emails = emailClient.GetEmails();
                int count = emails.Count;
                string message_subject;
                string message_date;
                bool valid_eol = true;
                bool is_empty_file = false;
                log.Info($"There are {count} emails");
                for (int i = 0; i < count; i++)
                {
                    // move all files from current to archive folder
                    IFileRepository.MoveFile(config.DeltaFileLocation, config.ArchiveLocation);
                    var message = emails[i];
                    message_subject = message.Subject;
                    message_date = message.MessageDate.ToString();
                    if (message.Attachments != null && message.Attachments.Count > 0)
                    {
                        //save attachments
                         emailClient.SaveAttachments(message, config.DeltaFileLocation);
                        // deletes non csv files
                        IFileRepository.DeleteFiles(config.DeltaFileLocation);
                        // find end of line in delta file
                        endOfLine = IFileRepository.FindEOL(config.DeltaFileLocation);
                        if (endOfLine != "CRLF" && endOfLine != "LF") valid_eol = false;
                        is_empty_file = IFileRepository.IsFileEmpty(config.DeltaFileLocation);
                        if (is_empty_file)
                        {
                            log.Info($"The attachement is empty");
                            ISQLRepository.CallProcedure(config.ConnString, "usp_DeltaFile_EmptyFileNotification");
                        }
                    }
                    log.Info($"End of line is {endOfLine}");
                    // call sql procedures if subject contains word from configuration
                    if (message.Subject.Contains(config.MailSubject) && valid_eol && !is_empty_file)
                    {

                        log.Info("Processing email " + message_subject + " from " + message_date);
                        ISQLRepository.CallProcedure(config.ConnString, "usp_DeltaFile_Wrapper", endOfLine);
                    }

                    emailClient.DeleteMessege(i);
                    log.Info("Deletion of email " + message_subject + " from " + message_date);
                }
                emailClient.Disconnect();
                log.Info("Finished");
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
            
        }
    }
}
