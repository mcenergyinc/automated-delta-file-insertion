﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;

namespace deltafile_mailkit
{
    public class ISQLRepository
    {
        public static void CallProcedure (string conn_name, string sp_name, string eol)
        {
            using (SqlConnection conn = new SqlConnection(conn_name))
            {
                conn.Open();

                SqlCommand sqlComm = new SqlCommand(sp_name,conn);
                sqlComm.Parameters.Add("@EndOfLine", SqlDbType.VarChar).Value = eol;
                sqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                sqlComm.ExecuteNonQuery();
            }

        }

        public static void CallProcedure(string conn_name, string sp_name)
        {
            using (SqlConnection conn = new SqlConnection(conn_name))
            {
                conn.Open();

                SqlCommand sqlComm = new SqlCommand(sp_name, conn);
                sqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                sqlComm.ExecuteNonQuery();
            }

        }
    }
}
