﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace deltafile_mailkit
{
    class IFileRepository
    {
        public static void MoveFile(string FileSourceFolder, string FileDestinationFolder)
        {
            foreach (string file in Directory.EnumerateFiles(FileSourceFolder).Select(Path.GetFileName))
            {
                string destFile = FileDestinationFolder + file;
                // not sure if this is ok approach, but first will delte if file exists in destination and then copy it again
                if (File.Exists(destFile))
                {
                    File.Delete(destFile);
                }
             
                File.Move(FileSourceFolder+ file, FileDestinationFolder+file);
                
            }
        }

        public static string FindEOL(string FileSourceFolder)
        {
            string endofLine = "";
            if(Directory.EnumerateFiles(FileSourceFolder).Count() != 1)
            {
                throw new InvalidOperationException("Too many csv files!");
            }

            string file = Directory.EnumerateFiles(FileSourceFolder).Select(Path.GetFileName).FirstOrDefault();
            StreamReader reader;
            reader = new StreamReader(FileSourceFolder+file);
            do
            {
                bool DoExit = false;
                int i = reader.Read();
                 // check if character is CRLF or LF
                if (i == 13)
                {
                    endofLine = "CRLF";
                    DoExit = true;
                }
                if (i == 10)
                {
                    endofLine = "LF";
                    DoExit = true;
                }
                if (DoExit) break;
               
            } while (!reader.EndOfStream);
            reader.Close();
            reader.Dispose();
            return endofLine;
        }
        /// <summary>
        /// deletes all attachments that might not be delta file in csv format
        /// </summary>
        public static void DeleteFiles(string FileSourceFolder)
        {
            DirectoryInfo di = new DirectoryInfo(FileSourceFolder);
            FileInfo[] files = di.GetFiles("*.*")
                                 .Where(p => p.Extension != ".csv").ToArray();
            foreach (FileInfo file in files)
               {
                    file.Attributes = FileAttributes.Normal;
                    File.Delete(file.FullName);
                }
        }

        public static bool IsFileEmpty(string FileSourceFolder)
        {
            if (Directory.EnumerateFiles(FileSourceFolder).Count() != 1)
            {
                throw new InvalidOperationException("Too many csv files!");
            }

            bool is_empty = false;
            string file = Directory.EnumerateFiles(FileSourceFolder).Select(Path.GetFileName).FirstOrDefault();
            FileInfo fi = new FileInfo(FileSourceFolder + file);
            if (fi.Length == 0) is_empty = true;
            return is_empty;


        }
    }
}
