﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MimeKit;
using System.IO;

namespace deltafile_mailkit
{
    public class MailRepository : IMailInterface
    {
        public void Connect(string hostname, string username, string password, int port, bool isUseSsl)
        {
            _emailClient = new MailKit.Net.Pop3.Pop3Client();
            _emailClient.Connect(hostname, port, isUseSsl);
            _emailClient.Authenticate(username, password);

        }

        public void DeleteMessege(int msgID)
        {
            _emailClient.DeleteMessage(msgID);
        }

        public List<EmailMessage> GetEmails()
        {
            List<EmailMessage> emails = new List<EmailMessage>();
            for (int i = 0; i < _emailClient.Count; i++)
            {
                var message = _emailClient.GetMessage(i);
                var test = message.Attachments;
                var attachments = message.BodyParts.OfType<MimePart>().Where(part => !string.IsNullOrEmpty(part.FileName));
                var emailMessage = new EmailMessage
                {
                    Content = !string.IsNullOrEmpty(message.HtmlBody) ? message.HtmlBody : message.TextBody,
                    Subject = message.Subject,
                    Attachments = message.BodyParts.OfType<MimePart>().Where(part => !string.IsNullOrEmpty(part.FileName)).ToList()
                    //Attachments = message.BodyParts.OfType<MessagePart>().Where(part => !string.IsNullOrEmpty(part.FileName)).ToList()
                };
                emailMessage.ToAddresses.AddRange(message.To.Select(x => (MailboxAddress)x).Select(x => new EmailAddress { Address = x.Address, Name = x.Name }));
                emailMessage.FromAddresses.AddRange(message.From.Select(x => (MailboxAddress)x).Select(x => new EmailAddress { Address = x.Address, Name = x.Name }));
                emailMessage.MessageDate = message.Date;
                emailMessage.MessageNumber = i;
                emails.Add(emailMessage);
              
            }
            return emails;

        }

        public void Disconnect()
        {
            _emailClient.Disconnect(true);

        }

        public void SaveAttachments (EmailMessage message, string location)
        {
            foreach (var attachment in message.Attachments)
            {
                    var part = (MimePart)attachment;
                    var fileName = part.FileName;

                    using (var stream = File.Create(location + fileName))
                        part.Content.DecodeTo(stream);
                
            }
        }

        private MailKit.Net.Pop3.Pop3Client _emailClient { get; set; } 


    }

    public class EmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }

    public class EmailMessage
    {
        public EmailMessage()
        {
            ToAddresses = new List<EmailAddress>();
            FromAddresses = new List<EmailAddress>();
        }

        public List<EmailAddress> ToAddresses { get; set; }
        public List<EmailAddress> FromAddresses { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public DateTimeOffset MessageDate { get; set; }
        public List<MimePart> Attachments { get; set; }
        public int MessageNumber { get; set; }

    }



}

