﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace deltafile_mailkit
{
    public class Config
    {
        public string DeltaFileLocation { get; set; }
        public string ArchiveLocation { get; set; }
                
        public string MailHost { get; set; }
        public int MailPort { get; set; }
        public string MailUser { get; set; }
        public string MailPwd { get; set; }
        public string MailSubject { get; set; }

        public string ConnString { get; set; }

        public void GetConfiguration()
       {
            DeltaFileLocation = ConfigurationManager.AppSettings["delta_file_location"];
            ArchiveLocation = ConfigurationManager.AppSettings["archive_location"];

            MailHost = ConfigurationManager.AppSettings["mail_host"];
            MailPort = int.Parse(ConfigurationManager.AppSettings["mail_port"]);
            MailUser = ConfigurationManager.AppSettings["mail_user"];
            MailPwd = ConfigurationManager.AppSettings["mail_pwd"];
            MailSubject = ConfigurationManager.AppSettings["mail_subject"];
            ConnString = GetConnectionStringByName("DbConn");

        }

        private string GetConnectionStringByName(string name)
        {
            string returnValue = null;
            ConnectionStringSettings setting = ConfigurationManager.ConnectionStrings[name];
            if (setting != null)
                returnValue = setting.ConnectionString;
                    
            return returnValue;

        }

    }
}
