USE [MCEnergyDBV2]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeltaFile_InsertionReport]    Script Date: 12/4/2018 10:43:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/****************************************************************************************************/
/*History																							*/
/*Date			Author		Description																*/
/*2018-10-11	V Mijovic	Created																	*/ 
/****************************************************************************************************/

ALTER PROCEDURE  [dbo].[usp_DeltaFile_InsertionReport] 

AS
BEGIN

SET NOCOUNT ON
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


-- prepare data for email
select distinct invoice_id, account_id
into #main
from MCEnergyDBV2.dbo.DeltaFile
where convert(date, created_date) = convert(date, getdate())

-- prepare voyager code
select invoice_id, account_id, value as voyager_proprty_code
into #voyager_code
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Voyager Property Code'
and convert(date, created_date) = convert(date, getdate())

-- prepare Vendor Account Number
select invoice_id, account_id, value as vendor_account_number
into #vendor_account_number
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Vendor Account Number'
and convert(date, created_date) = convert(date, getdate())

--prepare energy type
select invoice_id, account_id, value as energy_type
into #energy_type
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Energy Type'
and convert(date, created_date) = convert(date, getdate())

--prepare billing period start
select invoice_id, account_id, value as billing_period_start
into #bill_period_start
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Bill Period Start'
and convert(date, created_date) = convert(date, getdate())

--prepare billing period end
select invoice_id, account_id, value as billing_period_end
into #bill_period_end
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Bill Period End'
and convert(date, created_date) = convert(date, getdate())

--prepare Total Consumption
select invoice_id, account_id, value as Total_Consumption
into #Total_Consumption
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Total Consumption'
and convert(date, created_date) = convert(date, getdate())

--prepare demand
select invoice_id, account_id, value as demand
into #demand
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'demand'
and convert(date, created_date) = convert(date, getdate())

select invoice_id, account_id, value as total_cost
into #total_cost
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Total Cost'
and convert(date, created_date) = convert(date, getdate())

select invoice_id, account_id, value as Energy_Supply_Charge
into #Energy_Supply_Charge
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Energy Supply Charge'
and convert(date, created_date) = convert(date, getdate())

select invoice_id, account_id, value as total_water_cost
into #total_water_cost
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Water Cost'
and convert(date, created_date) = convert(date, getdate())

select invoice_id, account_id, value as total_sewer_cost
into #total_sewer_cost
from MCEnergyDBV2.dbo.DeltaFile
where value_type = 'Sewer Cost'
and convert(date, created_date) = convert(date, getdate())


truncate table [DeltaFile_InsertionReport]

INSERT INTO [DeltaFile_InsertionReport]
(invoice_id, client, property, voyager_property_code, energy_account_id, vendor_account_number, energy_type
, billing_period_start, billing_period_end, total_consumption, demand, total_cost, Energy_Supply_Charge, total_water_cost, total_sewer_cost)
select m.invoice_id,replace(isnull(cc.friendly_name,''),',',''), p.friendly_name, voyager_proprty_code, m.account_id,
vendor_account_number,energy_type,  billing_period_start, billing_period_end, [total_consumption], demand, total_cost, Energy_Supply_Charge, total_water_cost, total_sewer_cost
from #main m
inner join #voyager_code vc
on m.account_id = vc.account_id
and m.invoice_id = vc.invoice_id
inner join #bill_period_start bps
on m.account_id = bps.account_id
and m.invoice_id = bps.invoice_id
inner join #bill_period_end bpe
on m.account_id = bpe.account_id
and m.invoice_id = bpe.invoice_id
inner join #vendor_account_number van
on m.account_id = van.account_id
and m.invoice_id = van.invoice_id
inner join #energy_type et
on m.account_id = et.account_id
and m.invoice_id = et.invoice_id
inner join #Total_Consumption tcons
on m.account_id = tcons.account_id
and m.invoice_id = tcons.invoice_id
inner join #total_cost tc
on m.account_id = tc.account_id
and m.invoice_id = tc.invoice_id
inner join EnergyAccounts ea
on ea.energy_account_uid = m.account_id
inner join Properties p on p.property_uid = ea.property_uid and p.is_active=1
--inner join PropertyMeta pm on p.property_uid=pm.property_uid
--and pm.meta_value not in (1,3) 
--inner join PropertyMetaTypes pmt on pm.meta_type_uid=pmt.meta_type_uid
--inner join PropertyMetaChoices pmc 
--on pm.meta_type_uid=pmc.meta_type_uid and (pm.meta_value = pmc.meta_value or pm.meta_value = '')
--and pm.meta_type_uid='F6780985-9C71-4A9F-B309-335240F92CDB'
inner join Properties_ClientCompanies pcc 
on pcc.property_uid = p.property_uid 
and pcc.is_active=1 
and pcc.company_role_uid='FF8A2405-B3A2-4641-A021-98672448AFCC'
and pcc.relationship_end is null
left join ClientCompanies cc on cc.client_company_uid=pcc.client_company_uid
ANd cc.is_active=1 and cc.company_type_code in ('client', 'vendor', 'client_company')
left join #Energy_Supply_Charge esc
on m.account_id = esc.account_id
and m.invoice_id = esc.invoice_id
left join #demand d
on m.account_id = d.account_id
and m.invoice_id = d.invoice_id
left join #total_water_cost twc
on m.account_id = twc.account_id
and m.invoice_id = twc.invoice_id
left join #total_sewer_cost tsc
on m.account_id = tsc.account_id
and m.invoice_id = tsc.invoice_id


--select * from EnergyUsage where created_by = 'bouserit'

if object_id('tempdb..#main') is not null drop table #main
if object_id('tempdb..#voyager_code') is not null drop table #voyager_code
if object_id('tempdb..#bill_period_start') is not null drop table #bill_period_start
if object_id('tempdb..#bill_period_end') is not null drop table #bill_period_end
if object_id('tempdb..#bill_period_end') is not null drop table #bill_period_end
if object_id('tempdb..#vendor_account_number') is not null drop table #vendor_account_number
if object_id('tempdb..#energy_type') is not null drop table #energy_type
if object_id('tempdb..#Total_Consumption') is not null drop table #Total_Consumption
if object_id('tempdb..#Demand') is not null drop table #Demand
if object_id('tempdb..#Total_Cost') is not null drop table #Total_Cost
if object_id('tempdb..#Energy_Supply_Charge') is not null drop table #Energy_Supply_Charge
if object_id('tempdb..#total_water_cost') is not null drop table #total_water_cost
if object_id('tempdb..#total_sewer_cost') is not null drop table #total_sewer_cost

-- prepare an email

declare @qry nvarchar(1000)
declare @columnname varchar(50)


if (select count(*) from MCEnergyDBV2.dbo.[DeltaFile_InsertionReport]) = 0
	Begin
		EXEC msdb.dbo.sp_send_dbmail 
				@profile_name = 'SQLEmail',
				--@recipients= 'powershopping_datasupport@yardi.com;dba@xogito.com',
				--@recipients='Daniel.Rice@yardi.com;Diane.Lagana@yardi.com;Jonathan.Chiu@yardi.com;chris.smith@yardi.com;jaxel.rojas@yardi.com;dba@xogito.com',
				@recipients= 'powershopping_datasupport@yardi.com;dba@xogito.com',
				@subject = 'Daily Insertion Delta File Report',
				@body = 'There are no invoices that were successfully inserted today, please review exception report'

	end
else
	BEGIN

		-- Create the column name with the instrucation in a variable
		SET @ColumnName = '[sep=,' + CHAR(13) + CHAR(10) + 'invoice_id]'

		set @qry = 'set nocount on; select invoice_id ' + @ColumnName +
		', client, property, voyager_property_code, energy_account_id, vendor_account_number, energy_type, billing_period_start, billing_period_end, total_consumption, demand, Energy_Supply_Charge 
		, total_water_cost As [Water Cost], total_sewer_cost AS [Sewer Cost], total_cost AS [Total Cost]
		from MCEnergyDBV2.dbo.DeltaFile_InsertionReport order by client, property '

		EXEC msdb.dbo.sp_send_dbmail 
					@profile_name = 'SQLEmail',
					--@recipients= 'powershopping_datasupport@yardi.com;dba@xogito.com',
					@recipients= 'powershopping_datasupport@yardi.com;dba@xogito.com',
					@subject = 'Daily Insertion Delta File Report',
					@body = 'Please see details about insertions in attachement',
					@attach_query_result_as_file = 1,
					@query_attachment_filename = 'Daily_Insertion_Delta_File_Report.csv',
					@query = @qry ,
					@query_result_separator = ',',
					@query_result_no_padding = 1,
					@query_result_width =32767;

	END
			

SET NOCOUNT OFF
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
END






