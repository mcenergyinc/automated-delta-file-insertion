USE [MCEnergyDBV2]
GO

/****** Object:  StoredProcedure [dbo].[usp_DeltaFile_Wrapper]    Script Date: 12/4/2018 7:53:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/****************************************************************************************************/
/*History																							*/
/*Date			Author		Description																*/
/*2018-10-11	V Mijovic	Created																	*/ 
/****************************************************************************************************/

CREATE PROCEDURE  [dbo].[usp_DeltaFile_Wrapper] 
@EndOfLine varchar(10)
AS
BEGIN

SET NOCOUNT ON
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
-- to call all other procedures

exec [usp_DeltaFile_Insert] @EndOfLine;
exec [usp_DeltaFile_Process];
exec [usp_DeltaFile_InsertionReport]
exec [usp_DeltaFile_ExceptionReport]


SET NOCOUNT OFF
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
END







GO

