USE [MCEnergyDBV2]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeltaFile_ExceptionReport]    Script Date: 12/4/2018 10:42:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/****************************************************************************************************/
/*History																							*/
/*Date			Author		Description																*/
/*2018-10-11	V Mijovic	Created																	*/ 
/****************************************************************************************************/

ALTER PROCEDURE  [dbo].[usp_DeltaFile_ExceptionReport] 

AS
BEGIN

SET NOCOUNT ON
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


-- prepare data for email
-- HAVE TO HANDLE exception "Non existing account uid" a little bit different since there is no account to join energyaccount
select distinct invoice_id, account_id, exception_description, property
into #main
from MCEnergyDBV2.dbo.DeltaFile_exception
where convert(date, created_date) = convert(date, getdate())

-- prepare voyager code
select invoice_id, account_id, value as voyager_proprty_code
into #voyager_code
from MCEnergyDBV2.dbo.DeltaFile_exception
where value_type = 'Voyager Property Code'
and convert(date, created_date) = convert(date, getdate())

-- prepare voyager code
select invoice_id, account_id, value as vendor_account_number
into #vendor_account_number
from MCEnergyDBV2.dbo.DeltaFile_exception
where value_type = 'Vendor Account Number'
and convert(date, created_date) = convert(date, getdate())

--prepare energy type
select invoice_id, account_id, value as energy_type
into #energy_type
from MCEnergyDBV2.dbo.DeltaFile_exception
where value_type = 'Energy Type'
and convert(date, created_date) = convert(date, getdate())

--prepare billing period start
select invoice_id, account_id, value as billing_period_start
into #bill_period_start
from MCEnergyDBV2.dbo.DeltaFile_exception
where value_type = 'Bill Period Start'
and convert(date, created_date) = convert(date, getdate())

--prepare billing period end
select invoice_id, account_id, value as billing_period_end
into #bill_period_end
from MCEnergyDBV2.dbo.DeltaFile_exception
where value_type = 'Bill Period End'
and convert(date, created_date) = convert(date, getdate())

truncate table [DeltaFile_exceptionReport]
 
 -- first insert not existing account uid 
INSERT INTO [DeltaFile_exceptionReport]
(invoice_id,  property, voyager_property_code, energy_account_id, vendor_account_number, energy_type, billing_period_start, billing_period_end, exception_description)
select m.invoice_id, Property, voyager_proprty_code, m.account_id,
vendor_account_number,energy_type,  billing_period_start, billing_period_end, exception_description
from #main m
inner join #voyager_code vc
on m.account_id = vc.account_id
and m.invoice_id = vc.invoice_id
and m.Exception_description = 'Non existing account uid'
inner join #bill_period_start bps
on m.account_id = bps.account_id
and m.invoice_id = bps.invoice_id
inner join #bill_period_end bpe
on m.account_id = bpe.account_id
and m.invoice_id = bpe.invoice_id
inner join #vendor_account_number van
on m.account_id = van.account_id
and m.invoice_id = van.invoice_id
inner join #energy_type et
on m.account_id = et.account_id
and m.invoice_id = et.invoice_id
-- to be sure to skip this record
delete from #main where Exception_description = 'Non existing account uid'


INSERT INTO [DeltaFile_exceptionReport]
(invoice_id, client, property, voyager_property_code, energy_account_id, vendor_account_number, energy_type, account_type,  billing_period_start, billing_period_end, exception_description)
select distinct m.invoice_id, replace(isnull(cc.friendly_name,''),',',''), replace(Property,',',''), voyager_proprty_code, m.account_id,
vendor_account_number,energy_type, eat.friendly_name as account_type,  billing_period_start, billing_period_end, exception_description
from #main m
inner join #voyager_code vc
on m.account_id = vc.account_id
and m.invoice_id = vc.invoice_id
inner join #bill_period_start bps
on m.account_id = bps.account_id
and m.invoice_id = bps.invoice_id
inner join #bill_period_end bpe
on m.account_id = bpe.account_id
and m.invoice_id = bpe.invoice_id
inner join #vendor_account_number van
on m.account_id = van.account_id
and m.invoice_id = van.invoice_id
inner join #energy_type et
on m.account_id = et.account_id
and m.invoice_id = et.invoice_id
inner join EnergyAccounts ea
on ea.energy_account_uid = m.account_id
left join EnergyAccountTypes eat
on ea.account_type_uid = eat.account_type_uid
inner join Properties p on p.property_uid = ea.property_uid and p.is_active=1
inner join Properties_ClientCompanies pcc 
on pcc.property_uid = p.property_uid 
and pcc.is_active=1 
and pcc.company_role_uid='FF8A2405-B3A2-4641-A021-98672448AFCC'
and pcc.relationship_end is null
inner join ClientCompanies cc on cc.client_company_uid=pcc.client_company_uid
ANd cc.is_active=1 and cc.company_type_code in ('client', 'vendor', 'client_company')
--inner join PropertyMeta pm on p.property_uid=pm.property_uid
--inner join PropertyMetaTypes pmt on pm.meta_type_uid=pmt.meta_type_uid
--inner join PropertyMetaChoices pmc on 
--pm.meta_type_uid=pmc.meta_type_uid and (pm.meta_value = pmc.meta_value or pm.meta_value = '')
--and pm.meta_type_uid='F6780985-9C71-4A9F-B309-335240F92CDB'
--and pm.meta_value not in (1,3) 

if object_id('tempdb..#main') is not null drop table #main
if object_id('tempdb..#voyager_code') is not null drop table #voyager_code
if object_id('tempdb..#bill_period_start') is not null drop table #bill_period_start
if object_id('tempdb..#bill_period_end') is not null drop table #bill_period_end
if object_id('tempdb..#bill_period_end') is not null drop table #bill_period_end
if object_id('tempdb..#vendor_account_number') is not null drop table #vendor_account_number
if object_id('tempdb..#energy_type') is not null drop table #energy_type

-- prepare an email

declare @qry nvarchar(1000)
declare @columnname varchar(50)

	
if (select count(*) from MCEnergyDBV2.dbo.DeltaFile_exceptionReport) = 0
	Begin
		EXEC msdb.dbo.sp_send_dbmail 
				@profile_name = 'SQLEmail',
				@recipients= 'powershopping_datasupport@yardi.com;dba@xogito.com',
				--@recipients='Daniel.Rice@yardi.com;Diane.Lagana@yardi.com;Jonathan.Chiu@yardi.com;chris.smith@yardi.com;jaxel.rojas@yardi.com;dba@xogito.com',
				--@recipients= 'chris.smith@yardi.com;dba@xogito.com',
				@subject = 'Daily Exception Delta File Report',
				@body = 'There are no exceptions for today''s import'

	end
else
	begin
		SET @ColumnName = '[sep=,' + CHAR(13) + CHAR(10) + 'invoice_id]'

		set @qry = 'set nocount on; select invoice_id ' + @ColumnName +
		',client, property, voyager_property_code, energy_account_id, vendor_account_number,energy_type,account_type,  billing_period_start, billing_period_end, exception_description 
		from MCEnergyDBV2.dbo.DeltaFile_exceptionReport order by exception_description, client, property, vendor_account_number, billing_period_start,  energy_type'

		EXEC msdb.dbo.sp_send_dbmail 
					@profile_name = 'SQLEmail',
					--@recipients= 'powershopping_datasupport@yardi.com;dba@xogito.com',
					--@recipients='Daniel.Rice@yardi.com;Diane.Lagana@yardi.com;Jonathan.Chiu@yardi.com;chris.smith@yardi.com;jaxel.rojas@yardi.com;dba@xogito.com',
					@recipients= 'powershopping_datasupport@yardi.com;dba@xogito.com',
					@subject = 'Daily Exception Delta File Report',
					@body = 'Please see details about exceptions in attachement',
					@attach_query_result_as_file = 1,
					@query_attachment_filename = 'Daily_Exception_Delta_File_Report.csv',
					@query = @qry ,
					@query_result_separator = ',',
					@query_result_no_padding = 1,
					@query_result_width =32767;
	end


SET NOCOUNT OFF
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
END






