USE [MCEnergyDBV2]
GO

/****** Object:  StoredProcedure [dbo].[usp_DeltaFile_Insert]    Script Date: 2/19/2019 8:32:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/****************************************************************************************************/
/*History																							*/
/*Date			Author		Description																*/
/*2018-10-11	V Mijovic	Created																	*/ 
/*2019-02-19	V Mijovic	Added exception "No consumption unit of measure exist"					*/ 
/****************************************************************************************************/

CREATE PROCEDURE  [dbo].[usp_DeltaFile_Insert] 
@EndOfLine varchar(10)
AS
BEGIN

SET NOCOUNT ON
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
--======================================
-- use command shell to import a file 
--======================================
-- read file name from given location


DECLARE @v_cmdshell int
DECLARE @FilePath varchar(256)
DECLARE @importSQL nvarchar(1000)

CREATE TABLE  #FILE (FilePath varchar(256))

SELECT @v_cmdshell = state
FROM master.sys.system_components_surface_area_configuration
WHERE object_name = 'xp_cmdshell'

-- If xp_cmdshell isn't enabled, we will need to temporarily enable it now.
IF @v_cmdshell = 0
BEGIN
EXEC sp_configure 'show advanced options', 1
RECONFIGURE
EXEC sp_configure 'xp_cmdshell', 1
RECONFIGURE
END

INSERT INTO #FILE
exec xp_cmdshell 'dir Z:\Energy+_DeltaFile\Current\ /S /B'

-- If xp_cmdshell was disabled, turn it back off.
IF @v_cmdshell = 0
BEGIN
EXEC sp_configure 'show advanced options', 1
RECONFIGURE
EXEC sp_configure 'xp_cmdshell', 0
RECONFIGURE
END

DELETE FROM #FILE where FilePath is null

select  @FilePath =    filepath from #FILE

-- clean buffer table
TRUNCATE TABLE [DeltaFile_Buffer]

IF @EndOfLine = 'LF'
-- prepare sql command for insert into buffer table 
set @importSQL = 'BULK INSERT [dbo].[DeltaFile_Buffer] FROM ''' +@FilePath + '''
WITH (
formatfile = ''Z:\Energy+_DeltaFile\Code\deltafileLF.fmt''
, firstrow = 2
)'


IF @EndOfLine = 'CRLF'
-- prepare sql command for insert into buffer table 
set @importSQL = 'BULK INSERT [dbo].[DeltaFile_Buffer] FROM ''' +@FilePath + '''
WITH (
formatfile = ''Z:\Energy+_DeltaFile\Code\deltafileCRLF.fmt''
, firstrow = 2
)'
-- import file into table
exec sp_executesql @importSQL
-- remove "," from value column due to poorly formatted file.
update DeltaFile_Buffer set value = replace(value,'",','') where patindex('%",%', value) > 1

-- ======================================
-- temporary exclude water and sewer energy types
-- when Chris advise remove this part of code 
-- ==========================================
select distinct invoice_id into #temp from DeltaFile_Buffer where value_type = 'energy type'
and value in ('water', 'sewer')

delete d from DeltaFile_Buffer d
inner join #temp t
on d.invoice_id = t.invoice_id
-- ==============================================
--===============================================
-- exclude (filter out) records where voyager_property_code like 'sch%'  MD-1076. they are not supposed to be reported
-- =========================================
select distinct invoice_id
into #wrong_voyager_property_code
from DeltaFile_Buffer
where value_type = 'Voyager property code'
and value  like 'sch%'


-- clear final table as well as deltafile
DELETE b
FROM #wrong_voyager_property_code pc
INNER JOIN DeltaFile_Buffer b
ON pc.invoice_id = b.invoice_id
-- =======================================
-- end of exclude (filter out) records where voyager_property_code like 'sch%'  MD-1076
-- =======================================

-- =======================================
-- delete empty Consumption Unit of Measure
-- =======================================
-- first identify all invoices with more Consumption Unit of Measure
select invoice_id
into #double_CUOM
from DeltaFile_Buffer
where value_type = 'Consumption Unit of Measure' 
group by invoice_id
having count(*) >= 2 

delete b
from DeltaFile_Buffer b
inner join #double_CUOM d
on d.invoice_id = b.invoice_id
and b.value_type = 'Consumption Unit of Measure' and isnull(value,'') = ''

-- =======================================
-- end of delete empty Consumption Unit of Measure
-- =======================================

-- CHECK 1. try to identify non existing energy account uid
select distinct Invoice_Id 
into #non_existing 
from DeltaFile_Buffer db where not exists (select 1 from EnergyAccounts ea where ea.energy_account_uid = db.account_id)

-- move non existing account uid to exception table
INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description)
SELECT b.invoice_id, account_id, value_type, value, 'Non existing account uid'
FROM #non_existing ne
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = ne.invoice_id

-- clean buffer table
DELETE b
FROM #non_existing ne
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = ne.invoice_id
-- END of check 1

-- CHECK 2 if one account has duplicate value types. 
select distinct invoice_id
into #dups 
from [DeltaFile_Buffer] 
group by invoice_id, account_id, value_type
having count(*) > 1

-- remove from dups based on https://mcenergyinc.atlassian.net/browse/MD-1097
-- in order to import water/sewer for New York City Water Board
delete d
from #dups d
inner join [DeltaFile_Buffer] df
on d.invoice_id = df.invoice_id
inner join EnergyAccounts ea
on df.account_id = ea.energy_account_uid
inner join DeltaFile_EnergyMeta dfem
on ea.currentEnergyProviderUid = dfem.energy_provider_uid_check
and dfem.energy_plus_value in ('Water Cost','Sewer Cost')

-- move non existing account uid to exception table
INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT b.invoice_id, account_id, value_type, value, 'Duplicate values for same invoice id and account uid' as w, p.friendly_name
FROM #dups d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

-- clean buffer table 
DELETE b
FROM #dups d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
-- end of check 2

-- check 3 if invoice for New York City Water Board has different total consumption for water and sewer
-- if yes move to exception
select invoice_id
into #wrong_totalconsumption
from DeltaFile_Buffer df
inner join EnergyAccounts ea
on df.account_id = ea.energy_account_uid
and value_type = 'Total Consumption'
inner join DeltaFile_EnergyMeta dfem
on ea.currentEnergyProviderUid = dfem.energy_provider_uid_check
and dfem.energy_plus_value in ('Water Cost','Sewer Cost')
group by invoice_id
having count(distinct value) > 1

INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, account_id, value_type, value, 'Invoice ID contains different total consumption for water and sewer', p.friendly_name
FROM #wrong_totalconsumption d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

DELETE b
FROM #wrong_totalconsumption w
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = w.invoice_id
-- end of check 3

-- CHECK 4 energy type
-- before check for energy type,  delete sewer energy type from deltafile but only for New York City Water Board
-- md -1097 for details
delete df
from DeltaFile_Buffer df
inner join EnergyAccounts ea
on df.account_id = ea.energy_account_uid
and value_type = 'Energy Type'
and value = 'Sewer'
inner join DeltaFile_EnergyMeta dfem
on ea.currentEnergyProviderUid = dfem.energy_provider_uid_check
and dfem.energy_plus_value in ('Sewer Cost')

SELECT invoice_id, account_id, case when value = 'Electric' then 'Electricity' else value END as EnergyType
into #energytype
FROM DeltaFile_Buffer
where value_type = 'Energy Type'

-- check energy account
select distinct et.invoice_id
into #wrong_energytype
from #energytype et
inner join EnergyAccounts ea
on ea.energy_account_uid = et.account_id
inner join EnergyTypes mce_type
on mce_type.energy_type_uid = ea.energy_type_uid
where et.EnergyType <> mce_type.friendly_name

-- move wrong energy type to exception table 
INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, account_id, value_type, value, 'Wrong energy type for account uid', p.friendly_name
FROM #wrong_energytype d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

-- clean buffer table 
DELETE b
FROM #wrong_energytype d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
-- end of check 4

-- check 5 If Energy+ record has a some energy unit that needs to be moved to exceptions 
-- comment in https://mcenergyinc.atlassian.net/browse/MD-1098
-- split check in two steps, one for Demand UOM and the other for Consumption UOM
-- based on Chris's comments on May 22 at https://mcenergyinc.atlassian.net/browse/MD-1076 and MD-1098 the table 
-- DeltaFile_EnergyUnit has been updated and there is no record where move_to_exception = 1 so below code won't return anything
-- and let's keep the code if we have similar problem in the future
SELECT distinct invoice_id
INTO #invalid_CUOM
FROM DeltaFile_Buffer dfb
INNER JOIN DeltaFile_EnergyUnit df_eu
on dfb.value = df_eu.[energy_plus_unit]
AND df_eu.move_to_exception = 1
where value_type IN ('Consumption Unit of Measure')

INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, account_id, value_type, value, 'Consumption unit of measure is not valid', p.friendly_name
FROM #invalid_CUOM d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

DELETE b
FROM #invalid_CUOM d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id

/* https://mcenergyinc.atlassian.net/browse/MD-1076 "No consumption unit of measure exist" * */

SELECT distinct invoice_id
INTO #noexist_CUOM
FROM DeltaFile_Buffer dfb
where value_type IN ('Consumption Unit of Measure')
and isnull(value,'') = ''

INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, account_id, value_type, value, 'No consumption unit of measure exist', p.friendly_name
FROM #noexist_CUOM d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

DELETE b
FROM #noexist_CUOM d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
/* end of "No consumption unit of measure exist" */

-- demand unit of measure
SELECT distinct invoice_id
INTO #invalid_DUOM
FROM DeltaFile_Buffer dfb
INNER JOIN DeltaFile_EnergyUnit df_eu
on dfb.value = df_eu.[energy_plus_unit]
AND df_eu.move_to_exception = 1
where value_type IN ('Demand Unit of Measure')

INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, account_id, value_type, value, 'Demand unit of measure is not valid', p.friendly_name
FROM #invalid_DUOM d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

DELETE b
FROM #invalid_DUOM d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
-- end of check 5

-- check 6 if the record has already been inserted from delta file
select distinct dfb.invoice_id
into #already_imported
from DeltaFile_Buffer dfb
inner join DeltaFile df
on dfb.account_id = df.account_id
and dfb.invoice_id = df.invoice_id
and dfb.[value_type] = df.[value_type]

INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, account_id, value_type, value, 'Invioce_ID previously imported', p.friendly_name
FROM #already_imported d
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

DELETE b
FROM #already_imported ai
INNER JOIN DeltaFile_Buffer b
ON b.invoice_id = ai.invoice_id
-- end of check 6

---------------------------------

INSERT INTO [dbo].[DeltaFile] (invoice_id, account_id, value_type, value, is_processed)
select distinct invoice_id, account_id, value_type, value, 0 
from [DeltaFile_Buffer]


IF OBJECT_ID('tempdb..#non_existing') IS NOT NULL DROP TABLE #non_existing
IF OBJECT_ID('tempdb..#dups') IS NOT NULL DROP TABLE #dups
IF OBJECT_ID('tempdb..#file') IS NOT NULL DROP TABLE #file
IF OBJECT_ID('tempdb..#energytype') IS NOT NULL DROP TABLE #energytype
IF OBJECT_ID('tempdb..#wrong_energytype') IS NOT NULL DROP TABLE #wrong_energytype
IF OBJECT_ID('tempdb..#invalid_DUOM') IS NOT NULL DROP TABLE #invalid_DUOM
IF OBJECT_ID('tempdb..#invalid_CUOM') IS NOT NULL DROP TABLE #invalid_CUOM
IF OBJECT_ID('tempdb..#wrong_totalconsumption') IS NOT NULL DROP TABLE #wrong_totalconsumption
IF OBJECT_ID('tempdb..#already_imported') IS NOT NULL DROP TABLE #already_imported
IF OBJECT_ID('tempdb..#wrong_voyager_property_code') IS NOT NULL DROP TABLE #wrong_voyager_property_code
IF OBJECT_ID('tempdb..#temp') IS NOT NULL DROP TABLE #temp
IF OBJECT_ID('tempdb..#double_CUOM') IS NOT NULL DROP TABLE #double_CUOM
IF OBJECT_ID('tempdb..#noexist_CUOM') IS NOT NULL DROP TABLE #noexist_CUOM

			

SET NOCOUNT OFF
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
END







GO


