USE [MCEnergyDBV2]
GO

/****** Object:  StoredProcedure [dbo].[usp_DeltaFile_InsertionReport]    Script Date: 1/15/2019 11:27:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/****************************************************************************************************/
/*History																							*/
/*Date			Author		Description																*/
/*2019-01-15	V Mijovic	Created																	*/ 
/****************************************************************************************************/

CREATE PROCEDURE  [dbo].[usp_DeltaFile_EmptyFileNotification] 

AS
BEGIN

SET NOCOUNT ON
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


-- prepare an email

declare @qry nvarchar(1000)
declare @columnname varchar(50)


		EXEC msdb.dbo.sp_send_dbmail 
				@profile_name = 'SQLEmail',
				--@recipients= 'powershopping_datasupport@yardi.com;dba@xogito.com',
				@recipients= 'chris.smith@yardi.com;dba@xogito.com',
				@subject = 'Daily Insertion Delta File Report',
				@body = 'No Records in current MCEnergy Invioce Delta File'

SET NOCOUNT OFF
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
END






GO


