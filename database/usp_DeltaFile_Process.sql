USE [MCEnergyDBV2]
GO

/****** Object:  StoredProcedure [dbo].[usp_DeltaFile_Process]    Script Date: 12/4/2018 7:57:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/****************************************************************************************************/
/*History																							*/
/*Date			Author		Description																*/
/*2018-10-11	V Mijovic	Created																	*/ 
/****************************************************************************************************/

CREATE PROCEDURE  [dbo].[usp_DeltaFile_Process] 

AS
BEGIN

SET NOCOUNT ON
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/* table variable will be used for output command from energyusage to store energy_usage_uid */
CREATE TABLE #energyusage_id (energy_usage_uid uniqueidentifier, energy_account_uid uniqueidentifier, period_start datetime)
/* PREPARE DATA FROM DELTA FILE */
select distinct invoice_id, account_id
into #invoice
from DeltaFile
where is_processed = 0

SELECT invoice_id, account_id, case when value = 'Electric' then 'Electricity' else value END as EnergyType
into #energytype
FROM DeltaFile
where value_type = 'Energy Type'
and is_processed = 0

select invoice_id, account_id, convert(datetime,value) as period_start
into #bill_period_start
from DeltaFile
where value_type = 'Bill Period Start'
and is_processed = 0

select invoice_id, account_id, convert(datetime,value) as period_end
into #bill_period_end
from DeltaFile
where value_type = 'Bill Period End'
and is_processed = 0

select invoice_id, account_id, value as energy_plus_unit
into #UnitOfMeasure
from DeltaFile df
where value_type = 'Consumption Unit of Measure'
and is_processed = 0

select invoice_id, account_id, convert(decimal(19,6), value) as recorded_value
into #Total_Consumption
from DeltaFile
where value_type = 'Total Consumption'
and is_processed = 0

select invoice_id, account_id, convert(money,value) as total_cost
into #Total_Cost
from DeltaFile
where value_type = 'Total Cost'
and is_processed = 0

select invoice_id, account_id, value as voyager_code
into #voyager_code
from DeltaFile
where value_type = 'Voyager Property Code'
and is_processed = 0
-- END OF PREPARATION
-- MERGE TEMP TABLE INTO ONE BIG TABLE
select 
i.invoice_id
, i.account_id
, period_start
, period_end
,convert(uniqueidentifier, null) AS [energy_unit_uid] 
,'' AS [classification_code]
,recorded_value
, 0 AS [is_system_calculated]
, convert(uniqueidentifier, null) AS document_set_uid
, convert(uniqueidentifier, null) AS [emission_factor_uid] 
, 1  AS is_active
, 'bouserit' AS created_by
, getdate() AS created_date
, 'bouserit' AS modified_by
, getdate() AS modified_date
,total_cost
, 'actual'	AS [usage_type_code]
, 'ok'		AS [usage_status_code]
, getdate()	AS [usage_status_date]
, convert(datetime, null) as [report_date] -- calculation for this field is below
, getdate() AS [consider_touched_date]
, uom.energy_plus_unit
, et.EnergyType
, v.voyager_code
into #final
from #invoice i 
inner join #bill_period_start bps
on bps.account_id = i.account_id
and bps.invoice_id = i.invoice_id
inner join #bill_period_end bpe
on i.invoice_id = bpe.invoice_id
and i.account_id = bpe.account_id
inner join #Total_Consumption tcon
on i.invoice_id = tcon.invoice_id
and i.account_id = tcon.account_id
inner join #Total_Cost tcost
on i.invoice_id = tcost.invoice_id
and i.account_id = tcost.account_id
inner join #UnitOfMeasure uom
on i.invoice_id = uom.invoice_id
and i.account_id = uom.account_id
inner join #EnergyType et
on i.invoice_id = et.invoice_id
and i.account_id = et.account_id
inner join #voyager_code v
on i.invoice_id = v.invoice_id
and i.account_id = v.account_id

--=====================================
--"Billing period and all values are same as current record in energyusage" process
-- check all values. move to exception if they are the same
--=====================================
select f.invoice_id, f.account_id
into #same_values
from #final f
inner join EnergyUsage eu
on f.account_id = eu.energy_account_uid
and f.period_start = eu.period_start
and f.period_end = eu.period_end
and f.recorded_value = eu.recorded_value
and f.total_cost = eu.total_cost
and eu.usage_type_code = 'actual'

INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, b.account_id, value_type, value, 'Billing period and all values are same as current record in energyusage', p.friendly_name
FROM #same_values sv
INNER JOIN DeltaFile b
ON b.invoice_id = sv.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

delete df from DeltaFile df 
inner join #same_values sv
on df.account_id = sv.account_id
and df.invoice_id = sv.invoice_id

delete f from #final f 
inner join #same_values sv
on f.account_id = sv.account_id
and f.invoice_id = sv.invoice_id

-- ======================================
-- end checks for all values		 ---
---=====================================
-- checks for same period but different values
--===========================================
select f.invoice_id, f.account_id
into #different_values
from #final f
inner join EnergyUsage eu
on f.account_id = eu.energy_account_uid
and f.period_start = eu.period_start
and f.period_end = eu.period_end
and eu.usage_type_code = 'actual'
and (f.recorded_value != eu.recorded_value
or f.total_cost != eu.total_cost)


INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, b.account_id, value_type, value, 'Billing period same as current record in energyusage but values differ from existing record', p.friendly_name
FROM #different_values sv
INNER JOIN DeltaFile b
ON b.invoice_id = sv.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

delete df from DeltaFile df 
inner join #different_values dv
on df.account_id = dv.account_id
and df.invoice_id = dv.invoice_id

delete f from #final f 
inner join #different_values dv
on f.account_id = dv.account_id
and f.invoice_id = dv.invoice_id
--=======================================
-- end checks for different values ---
---=====================================
---  checks for overlaping			---
--======================================
SELECT distinct account_id, invoice_id , f.period_start
into #overlap
FROM #final f
INNER JOIN [EnergyUsage] eu
on f.account_id = eu.energy_account_uid
and eu.usage_type_code = 'actual'
GROUP by account_id, invoice_id, f.period_start
having max(eu.period_end) > f.period_start

INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, b.account_id, value_type, value, 'Overlapping billing period of existing record', p.friendly_name
FROM #overlap d
INNER JOIN DeltaFile b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

delete df from DeltaFile df 
inner join #overlap ol
on df.account_id = ol.account_id
and df.invoice_id = ol.invoice_id

delete f from #final f 
inner join #overlap ol
on f.account_id = ol.account_id
and f.invoice_id = ol.invoice_id
-- ======================================
-- end checks for overlaping		 ---
-- ======================================
-- check long period			     ---
-- ======================================
-- if difference between start and end date is more than 40 days insert into exception table
select distinct invoice_id
into #longperiod
from #final where DATEDIFF(day, period_start, period_end) >= 40
-- insert into exception 
INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, account_id, value_type, value, 'Difference between bill period start and bill period end in Energy+ is 40 days or greater', p.friendly_name
FROM #longperiod d
INNER JOIN DeltaFile b
ON b.invoice_id = d.invoice_id
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

-- clear final table as well as deltafile
DELETE f
FROM #longperiod lp
INNER JOIN #final f
ON lp.invoice_id = f.invoice_id

DELETE df
FROM #longperiod lp
INNER JOIN DeltaFile df
ON lp.invoice_id = df.invoice_id
-- ======================================
-- end long period			     ---
-- ======================================
-- calculate report date for existing records . keep in mind that it might be that some accounts have more than one invoice.
-- ============================
SELECT account_id, max(eu.report_date) As report_date 
into #existing_reportdate
FROM #final f
INNER JOIN [EnergyUsage] eu
on f.account_id = eu.energy_account_uid
and eu.usage_type_code = 'actual'
GROUP by account_id

-- Find invoices where report date is less equal than 2 months comparing to billing period start
-- first need to identify minimum billing period start for all accounts and then compare to report date
select min(convert(date,period_start)) as period_start, account_id 
into #first_period
from #final 
group by account_id
--========================================
--gap
--========================================
-- find periodend for the last report date and compare it to energy + period start in order to identify gap
select er.account_id, eu.period_end
into #last_periodend
from #existing_reportdate er
inner join [EnergyUsage] eu
on er.account_id = eu.energy_account_uid
and er.report_date = eu.report_date
and eu.usage_type_code = 'actual'

select distinct f.account_id, period_start
into #gap_report_date
from #last_periodend lpe
inner join #first_period f
on lpe.account_id = f.account_id
and datediff(DAY, convert(date,lpe.period_end), convert(date,period_start)) >=40

select distinct invoice_id, f.account_id, f.period_start, f.period_end
into #gap_report_date_calculate
from #gap_report_date grd
inner join #final f
on f.account_id = grd.account_id
and convert(date,grd.period_start) >= convert(date,f.period_start)

-- sort invoices per account order by billing period
-- but do not include gap invoices, they will be calculated as new accounts
select ROW_NUMBER () over (partition by account_id order by convert(date,period_start) ASC) as rn, 
invoice_id, account_id, period_start, period_end
into #calculated_reportdate
from #final f
where not exists (select 1 from #gap_report_date_calculate g where g.invoice_id = f.invoice_id)
order by account_id

update f
set report_date = dateadd(month, rn, erd.report_date)
--select f.account_id, f.invoice_id, erd.report_date, crd.rn, crd.period_start, crd.period_end 
from #final f
inner join #calculated_reportdate crd
on f.invoice_id = crd.invoice_id
inner join #existing_reportdate erd
on erd.account_id = f.account_id

--select account_id, period_start, period_end, report_date from #final order by period_start

-- ======================================
-- new account          ---
-- ======================================
-- find if there is any new account id
SELECT invoice_id,  account_id, period_start, period_end
, dateadd(day,- day(period_end)+1,period_end ) as period_end_firstday
,  EOMONTH(period_start) as period_begin_lastday
, convert(datetime,null) as report_date
into #missing_reportdate
FROM #final f where NOT EXISTS (SELECT 1 FROM EnergyUsage eu where eu.energy_account_uid = f.account_id)

-- add gapped invoices
insert into #missing_reportdate (invoice_id,  account_id, period_start, period_end, period_end_firstday, period_begin_lastday, report_date  )
select invoice_id,  account_id, period_start, period_end, dateadd(day,- day(period_end)+1,period_end ), EOMONTH(period_start), null
 from #gap_report_date_calculate

UPDATE #missing_reportdate
set report_date = case 
	-- period start and period end are in same month
	when MONTH(period_start) = MONTH(period_end) Then dateadd(day,- day(period_start)+1,period_start )
	-- more days in starting month than in next month
	when datediff(day, period_start,period_begin_lastday) > DATEDIFF(day, period_end_firstday, period_end)  and datediff(month, period_start, period_end) = 1
		then dateadd(day,- day(period_start)+1,period_start ) 
	-- more days in next month than in starting month
	when datediff(day, period_start, period_begin_lastday) <= DATEDIFF(day, period_end_firstday, period_end)  and datediff(month, period_start, period_end) = 1
		then period_end_firstday
	-- time span is in 3 consecutive months. Beginning of middle month is used for report date
	when datediff(month, period_start, period_end) = 2 then dateadd(day, 1, period_begin_lastday )
	END

update f
set report_date = m.report_date
from #final f
inner join #missing_reportdate m
on f.invoice_id = m.invoice_id
-- ======================================
-- end of calculation for reporting date
-- ======================================
-- find proper energy unit			  ---
-- ======================================
-- use energy unit uid from energyaccounts in column DeltaFile_EnergyUnit.check_in_energyaccount is 1
update f
set energy_unit_uid = ea.energy_unit_uid,
	recorded_value = recorded_value*multiplier
from #final f
inner join EnergyAccounts ea
on ea.energy_account_uid = f.account_id
inner join DeltaFile_EnergyUnit dfeu
on ea.energy_unit_uid = dfeu.energy_unit_uid
and dfeu.check_in_energyaccount = 1
and f.energy_plus_unit = dfeu.energy_plus_unit

-- use only mapping table
update f
set energy_unit_uid = dfeu.energy_unit_uid,
	recorded_value = recorded_value*multiplier
from #final f
inner join DeltaFile_EnergyUnit dfeu
on f.energy_plus_unit = dfeu.energy_plus_unit
and f.energy_unit_uid is null
and check_in_energyaccount = 0

-- check if energy unit uid is null
INSERT INTO [dbo].[DeltaFile_exception] (invoice_id, account_id, value_type, value, exception_description, property)
SELECT distinct b.invoice_id, b.account_id, value_type, value, 'Unfound Energy Unit of Measure', p.friendly_name
FROM #final de
INNER JOIN DeltaFile b
ON b.invoice_id = de.invoice_id
and de.energy_unit_uid is null
INNER JOIN EnergyAccounts ea
on ea.energy_account_uid = b.account_id
INNER JOIN Properties p
on p.property_uid = ea.property_uid

-- first delete from deltafile table 
DELETE df
FROM #final f
INNER JOIN DeltaFile df
ON f.invoice_id = df.invoice_id
and f.energy_unit_uid is null

DELETE FROM  #final where energy_unit_uid is null

-- ====================================
-- end of find proper energy unit
-- ======================================
-- prepare for meta table 
-- meta table will not be as table for energyusage since we need insert for each meta type
-- demand meta value
-- =======================================================================
select df.invoice_id, df.account_id, value as Demand, f.EnergyType, f.period_start
into #Demand
from DeltaFile df
inner join #final f
on df.account_id = f.account_id
and df.invoice_id = f.invoice_id
where value_type = 'Demand'
and is_processed = 0

-- find meta_type_uid. 
-- IMPORTANT: IF energy type gas, value type Demand in energy +, and energy provider is not B6451F32-983C-4B10-ABB4-63DFCD435612 delete 
-- from demands but import into energyusage
delete d
from #Demand d
inner join DeltaFile_EnergyMeta df_dm
on d.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Demand'
and df_dm.energy_plus_type = 'Gas'
inner join EnergyAccounts ea
on ea.energy_account_uid = d.account_id
and ea.currentEnergyProviderUid != df_dm.energy_provider_uid_check

-- first find meta type uid if energy provider is the same as in mapping table and afterwards for different provider
select distinct d.invoice_id, d.account_id, d.Demand, df_dm.meta_type_uid, df_dm.energy_unit_id, d.period_start
into #demand_meta
from #demand d
inner join DeltaFile_EnergyMeta df_dm
on d.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Demand'
inner join EnergyAccounts ea
on ea.energy_account_uid = d.account_id
and ea.currentEnergyProviderUid = df_dm.energy_provider_uid_check

-- add records that do not have energy provider from mapping table
insert into #demand_meta (invoice_id, account_id, demand, meta_type_uid, energy_unit_id, period_start)
select distinct d.invoice_id, d.account_id, d.Demand, df_dm.meta_type_uid, df_dm.energy_unit_id, d.period_start
from #demand d
inner join DeltaFile_EnergyMeta df_dm
on d.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Demand'
and not exists (select 1 from #demand_meta dm where dm.invoice_id = d.invoice_id)
and df_dm.energy_provider_uid_check is null
-- end of demand meta value

-- Fuel Adjustment meta value
select df.invoice_id, df.account_id, value as Fuel_Adjustment, f.EnergyType, f.period_start
into #Fuel_Adjustment
from DeltaFile df
inner join #final f
on df.account_id = f.account_id
and df.invoice_id = f.invoice_id
where value_type = 'Fuel Adjustment'
and is_processed = 0

select distinct fa.invoice_id, fa.account_id, fa.Fuel_Adjustment, df_dm.meta_type_uid, df_dm.energy_unit_id, fa.period_start
into #Fuel_Adjustment_meta
from #Fuel_Adjustment fa
inner join DeltaFile_EnergyMeta df_dm
on fa.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Fuel Adjustment'
-- end of Fuel Adjustment meta value

-- Energy Supply Charge meta type
select df.invoice_id, df.account_id, value as Energy_Supply_Charge, f.EnergyType, f.period_start
into #Energy_Supply_Charge
from DeltaFile df
inner join #final f
on df.account_id = f.account_id
and df.invoice_id = f.invoice_id
where value_type = 'Energy Supply Charge'
and is_processed = 0

select distinct d.invoice_id, d.account_id, d.Energy_Supply_Charge, df_dm.meta_type_uid, df_dm.energy_unit_id, period_start
into #Energy_Supply_Charge_meta
from #Energy_Supply_Charge d
inner join DeltaFile_EnergyMeta df_dm
on d.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Energy Supply Charge'
inner join EnergyAccounts ea
on ea.energy_account_uid = d.account_id
and ea.currentEnergyProviderUid = df_dm.energy_provider_uid_check

-- add records that do not have energy provider from mapping table
insert into #Energy_Supply_Charge_meta (invoice_id, account_id, Energy_Supply_Charge, meta_type_uid, energy_unit_id, period_start)
select d.invoice_id, d.account_id, d.Energy_Supply_Charge, df_dm.meta_type_uid, df_dm.energy_unit_id, period_start
from #Energy_Supply_Charge d
inner join DeltaFile_EnergyMeta df_dm
on d.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Energy Supply Charge'
and not exists (select 1 from #Energy_Supply_Charge_meta dm where dm.invoice_id = d.invoice_id)
and df_dm.energy_provider_uid_check is null
-- end of Energy Supply Charge Meta

-- Total Water Cost $ meta type
select df.invoice_id, df.account_id, value as Total_Water_Cost, f.EnergyType, f.period_start
into #Total_Water_Cost
from DeltaFile df
inner join #final f
on df.account_id = f.account_id
and df.invoice_id = f.invoice_id
where value_type = 'Water Cost'
and is_processed = 0

select distinct d.invoice_id, d.account_id, d.Total_Water_Cost, df_dm.meta_type_uid, df_dm.energy_unit_id, period_start
into #Total_Water_Cost_meta
from #Total_Water_Cost d
inner join DeltaFile_EnergyMeta df_dm
on d.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Water Cost'
inner join EnergyAccounts ea
on ea.energy_account_uid = d.account_id
and ea.currentEnergyProviderUid = df_dm.energy_provider_uid_check
-- end of Total Water Cost $

-- Total Sewage Cost $ meta type
select df.invoice_id, df.account_id, value as Total_Sewer_Cost, f.EnergyType, f.period_start
into #Total_Sewage_Cost
from DeltaFile df
inner join #final f
on df.account_id = f.account_id
and df.invoice_id = f.invoice_id
where value_type = 'Sewer Cost'
and is_processed = 0

select distinct d.invoice_id, d.account_id, d.Total_Sewer_Cost, df_dm.meta_type_uid, df_dm.energy_unit_id, period_start
into #Total_Sewage_Cost_meta
from #Total_Sewage_Cost d
inner join DeltaFile_EnergyMeta df_dm
on d.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Sewer Cost'
inner join EnergyAccounts ea
on ea.energy_account_uid = d.account_id
and ea.currentEnergyProviderUid = df_dm.energy_provider_uid_check
-- end of Total Sewage Cost $

-- Energy Delivery Charge added on 2017-07-11
select df.invoice_id, df.account_id, value as Energy_Delivery_Charge, f.EnergyType, f.period_start
into #Energy_Delivery_Charge
from DeltaFile df
inner join #final f
on df.account_id = f.account_id
and df.invoice_id = f.invoice_id
where value_type = 'Energy Delivery Charge'
and is_processed = 0

-- find meta_type_uid. 
-- first find meta type uid if energy provider is the same as in mapping table and afterwards for different provider
select distinct edc.invoice_id, edc.account_id, edc.Energy_Delivery_Charge, df_dm.meta_type_uid, df_dm.energy_unit_id, edc.period_start
into #Energy_Delivery_Charge_meta
from #Energy_Delivery_Charge edc
inner join DeltaFile_EnergyMeta df_dm
on edc.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Energy Delivery Charge'
inner join EnergyAccounts ea
on ea.energy_account_uid = edc.account_id
and ea.currentEnergyProviderUid = df_dm.energy_provider_uid_check

-- add records that do not have energy provider from mapping table
insert into #Energy_Delivery_Charge_meta (invoice_id, account_id, Energy_Delivery_Charge, meta_type_uid, energy_unit_id, period_start)
select distinct edc.invoice_id, edc.account_id, edc.Energy_Delivery_Charge, df_dm.meta_type_uid, df_dm.energy_unit_id, edc.period_start
from #Energy_Delivery_Charge edc
inner join DeltaFile_EnergyMeta df_dm
on edc.EnergyType = df_dm.energy_plus_type
and df_dm.energy_plus_value = 'Energy Delivery Charge'
and not exists (select 1 from #Energy_Delivery_Charge_meta dm where dm.invoice_id = edc.invoice_id)
and df_dm.energy_provider_uid_check is null
-- end of Energy Delivery Charge meta value
-- end of all meta types
select * from #final 

-- INSERT STATEMENT 
BEGIN TRANSACTION

INSERT INTO [dbo].[EnergyUsage]
           ([energy_account_uid]
           ,[period_start]
           ,[period_end]
           ,[energy_unit_uid]
           ,[classification_code]
           ,[recorded_value]
           ,[is_system_calculated]
           ,[document_set_uid]
           ,[emission_factor_uid]
           ,[is_active]
           ,[created_by]
           ,[created_date]
           ,[modified_by]
           ,[modified_date]
           ,[total_cost]
           ,[usage_type_code]
           ,[usage_status_code]
           ,[usage_status_date]
           ,[report_date]
           ,[consider_touched_date])
OUTPUT INSERTED.energy_usage_uid, inserted.energy_account_uid, inserted.period_start INTO #energyusage_id (energy_usage_uid , energy_account_uid, period_start )
select		[account_id]
           ,[period_start]
           ,[period_end]
           ,[energy_unit_uid]
           ,[classification_code]
           ,[recorded_value]
           ,[is_system_calculated]
           ,[document_set_uid]
           ,[emission_factor_uid]
           ,[is_active]
           ,[created_by]
           ,[created_date]
           ,[modified_by]
           ,[modified_date]
           ,[total_cost]
           ,[usage_type_code]
           ,[usage_status_code]
           ,[usage_status_date]
           ,[report_date]
           ,[consider_touched_date]
		from #final
-- uid for user id reffers to bouser 
INSERT INTO [dbo].[EnergyUsageStatusHistory]
           ([status_history_uid]
           ,[energy_usage_uid]
           ,[user_uid]
           ,[usage_status_code]
           ,[usage_status_date]
          )
 select newid(), [energy_usage_uid],'44F83D8B-527E-4825-B401-A1D6889CA7DB', 'ok', getdate()
 from  #energyusage_id

 --insert into energyusagemeta DEMAND  meta type
INSERT INTO [dbo].[EnergyUsageMeta]
([energy_usage_uid], [meta_type_uid], [meta_value], [is_active], [created_by], [created_date], [modified_by], [modified_date],[energy_unit_uid])
SELECT eu.energy_usage_uid, dm.meta_type_uid, dm.Demand, 1, 'bouserit', getdate(), 'bouserit', getdate(), dm.energy_unit_id
FROM #energyusage_id eu
INNER JOIN #demand_meta dm
on eu.period_start = dm.period_start
and eu.energy_account_uid = dm.account_id

 --insert into energyusagemeta Fuel Adjustment  meta type
INSERT INTO [dbo].[EnergyUsageMeta]
([energy_usage_uid], [meta_type_uid], [meta_value], [is_active], [created_by], [created_date], [modified_by], [modified_date],[energy_unit_uid])
SELECT eu.energy_usage_uid, dm.meta_type_uid, dm.Fuel_Adjustment, 1, 'bouserit', getdate(), 'bouserit', getdate(), dm.energy_unit_id
FROM #energyusage_id eu
INNER JOIN #Fuel_Adjustment_meta dm
on eu.period_start = dm.period_start
and eu.energy_account_uid = dm.account_id

 --insert into energyusagemeta Energy Supply Charge  meta type
INSERT INTO [dbo].[EnergyUsageMeta]
([energy_usage_uid], [meta_type_uid], [meta_value], [is_active], [created_by], [created_date], [modified_by], [modified_date],[energy_unit_uid])
SELECT eu.energy_usage_uid, dm.meta_type_uid, dm.Energy_Supply_Charge, 1, 'bouserit', getdate(), 'bouserit', getdate(), dm.energy_unit_id
FROM #energyusage_id eu
INNER JOIN #Energy_Supply_Charge_meta dm
on eu.period_start = dm.period_start
and eu.energy_account_uid = dm.account_id

 --insert into energyusagemeta total water cost  meta type
INSERT INTO [dbo].[EnergyUsageMeta]
([energy_usage_uid], [meta_type_uid], [meta_value], [is_active], [created_by], [created_date], [modified_by], [modified_date],[energy_unit_uid])
SELECT eu.energy_usage_uid, dm.meta_type_uid, dm.Total_Water_Cost, 1, 'bouserit', getdate(), 'bouserit', getdate(), dm.energy_unit_id
FROM #energyusage_id eu
INNER JOIN #Total_Water_Cost_meta dm
on eu.period_start = dm.period_start
and eu.energy_account_uid = dm.account_id

 --insert into energyusagemeta total water cost  meta type
INSERT INTO [dbo].[EnergyUsageMeta]
([energy_usage_uid], [meta_type_uid], [meta_value], [is_active], [created_by], [created_date], [modified_by], [modified_date],[energy_unit_uid])
SELECT eu.energy_usage_uid, dm.meta_type_uid, dm.Total_Sewer_Cost, 1, 'bouserit', getdate(), 'bouserit', getdate(), dm.energy_unit_id
FROM #energyusage_id eu
INNER JOIN #Total_Sewage_Cost_meta dm
on eu.period_start = dm.period_start
and eu.energy_account_uid = dm.account_id

 --insert into energyusagemeta Energy Delivery Charge  meta type
INSERT INTO [dbo].[EnergyUsageMeta]
([energy_usage_uid], [meta_type_uid], [meta_value], [is_active], [created_by], [created_date], [modified_by], [modified_date],[energy_unit_uid])
SELECT eu.energy_usage_uid, dm.meta_type_uid, dm.Energy_Delivery_Charge, 1, 'bouserit', getdate(), 'bouserit', getdate(), dm.energy_unit_id
FROM #energyusage_id eu
INNER JOIN #Energy_Delivery_Charge_meta dm
on eu.period_start = dm.period_start
and eu.energy_account_uid = dm.account_id

-- update is_processed in deltafile table
update df
set is_processed = 1
from #final f
inner join DeltaFile df
on df.account_id = f.account_id
and df.invoice_id = f.invoice_id

/* ===============================
insert into DeltaFile_VoyagerCode
================================== */
-- first insert energy accounts that do not exist in DeltaFile_VoyagerCode
INSERT INTO DeltaFile_VoyagerCode
(energy_account_uid, voyager_property_code)
SELECT account_id, voyager_code FROM #final f where not exists (SELECT 1 FROM DeltaFile_VoyagerCode vc where f.account_id = vc.energy_account_uid)

-- delete columns that have same combination of energy accoun uid and voyager property code. this affects previously inserted records
delete f from #final f
inner join DeltaFile_VoyagerCode vc
on f.voyager_code = vc.voyager_property_code
and f.account_id = vc.energy_account_uid

-- join on energy account uid but different voyager property code
-- to mark them as inactive
-- all records from temp table #final should match that criteria
update vc
set is_active = 0,
	inactive_date = getdate()
from #final f
inner join DeltaFile_VoyagerCode vc
on f.account_id = vc.energy_account_uid
and f.voyager_code != vc.voyager_property_code

-- and then insert new voyager property code
INSERT INTO DeltaFile_VoyagerCode
(energy_account_uid, voyager_property_code)
SELECT account_id, voyager_code FROM #final -- there should not be duplicated records

-- 

if @@ERROR = 0
	begin
		Commit
		print 'commit'
	end
else
	begin
		rollback
		print 'rollback'
	end

if object_id('tempdb..#invoice') is not null drop table #invoice
if object_id('tempdb..#bill_period_start') is not null drop table #bill_period_start
if object_id('tempdb..#bill_period_end') is not null drop table #bill_period_end
if object_id('tempdb..#UnitOfMeasure') is not null drop table #UnitOfMeasure
if object_id('tempdb..#Total_Consumption') is not null drop table #Total_Consumption
if object_id('tempdb..#Total_Cost') is not null drop table #Total_Cost
if object_id('tempdb..#final') is not null drop table #final
if object_id('tempdb..#missing_reportdate') is not null drop table #missing_reportdate
if object_id('tempdb..#existing_reportdate') is not null drop table #existing_reportdate
if object_id('tempdb..#all_reportdate') is not null drop table #all_reportdate
if object_id('tempdb..#energytype') is not null drop table #energytype
if object_id('tempdb..#longperiod') is not null drop table #longperiod
if object_id('tempdb..#energyusage_id') is not null drop table #energyusage_id
if object_id('tempdb..#calculated_reportdate') is not null drop table #calculated_reportdate
if object_id('tempdb..#overlap') is not null drop table #overlap
if object_id('tempdb..#first_period') is not null drop table #first_period
if object_id('tempdb..#gap_report_date') is not null drop table #gap_report_date
if object_id('tempdb..#gap_report_date_calculate') is not null drop table  #gap_report_date_calculate
if object_id('tempdb..#voyager_code') is not null drop table  #voyager_code
if object_id('tempdb..#Demand') is not null drop table  #Demand
if object_id('tempdb..#Fuel_Adjustment') is not null drop table #Fuel_Adjustment
if object_id('tempdb..#demand_exception') is not null drop table  #demand_exception
if object_id('tempdb..#Energy_Supply_Charge') is not null drop table  #Energy_Supply_Charge
if object_id('tempdb..#Demand_meta') is not null drop table  #Demand_meta
if object_id('tempdb..#Fuel_Adjustment_meta') is not null drop table #Fuel_Adjustment_meta
if object_id('tempdb..#Energy_Supply_Charge_meta') is not null drop table #Energy_Supply_Charge_meta
if object_id('tempdb..#last_periodend') is not null drop table #last_periodend
if object_id('tempdb..#same_values') is not null drop table #same_values
if object_id('tempdb..#different_values') is not null drop table #different_values
if object_id('tempdb..#Total_Water_Cost') is not null drop table #Total_Water_Cost
if object_id('tempdb..#Total_Water_Cost_meta') is not null drop table #Total_Water_Cost_meta
if object_id('tempdb..#Total_Sewage_Cost') is not null drop table #Total_Sewage_Cost
if object_id('tempdb..#Total_Sewage_Cost_meta') is not null drop table #Total_Sewage_Cost_meta
if object_id('tempdb..#Energy_Delivery_Charge') is not null drop table #Energy_Delivery_Charge
if object_id('tempdb..#Energy_Delivery_Charge_meta') is not null drop table #Energy_Delivery_Charge_meta

			

SET NOCOUNT OFF
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
END







GO

